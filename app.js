$(document).ready(function() {

    const addIngredientEl = document.getElementById('add-ingredient');
    const ingredientEl = document.getElementById('ingredient');
    const submitIngredientEl = document.getElementById('submit-ingredient');
    const calculateEl = document.getElementById('calculate');
    const resetEl = document.getElementById('reset');

    let totalCarbsList = [];
    let totalSugarList = [];
    let totalProteinList = [];
    let totalSaturatedList = [];
    let totalCholesterolList = [];
    let totalSodiumList = [];
    let totalCarbs = 0;
    let totalSugars = 0;
    let totalProtein = 0; 
    let totalSaturated = 0;
    let totalCholesterol = 0;
    let totalSodium = 0;
    let apiCount = 0;

    class CalcNutrition {

        stageIngredient(ingredient) {
            const sIngredient = ingredient;
            const html = `
                <li class="list-group-item">
                    <span class="badge rounded-pill bg-secondary pull-left">${sIngredient}</span>
                </li>
            `
            return $('#stage-ingredient').append(html)
        }
        
        getTotalCarbs(responseJson) {
        
            if (responseJson.totalNutrients.CHOCDF) {
                let carb = responseJson.totalNutrients.CHOCDF.quantity;
                totalCarbsList.push(carb);
                apiCount ++;
                return totalCarbs = Math.ceil(totalCarbsList.reduce((a, b) => a + b)) 
            }         
        }

        getTotalSugar(responseJson) {

            if (responseJson.totalNutrients.SUGAR) {
                let sugar = responseJson.totalNutrients.SUGAR.quantity;
                totalSugarList.push(sugar);
                return totalSugars = Math.ceil(totalSugarList.reduce((a, b) => a + b))   
            } 
        }

        getTotalProtein(responseJson) {

            if (responseJson.totalNutrients.PROCNT) {
                let protein = responseJson.totalNutrients.PROCNT.quantity;
                totalProteinList.push(protein);
                return totalProtein = Math.ceil(totalProteinList.reduce((a, b) => a + b))   
            } 
        }

        getTotalSaturated(responseJson) {

            if (responseJson.totalNutrients.FASAT) {
                let saturated = responseJson.totalNutrients.FASAT.quantity;
                totalSaturatedList.push(saturated);
                return totalSaturated = Math.ceil(totalSaturatedList.reduce((a, b) => a + b))   
            } 
        }

        getTotalCholesterol(responseJson) {

            if (responseJson.totalNutrients.CHOLE) {
                let cholesterol = responseJson.totalNutrients.CHOLE.quantity;
                totalCholesterolList.push(cholesterol);
                return totalCholesterol = Math.ceil(totalCholesterolList.reduce((a, b) => a + b))   
            } 
        }

        getTotalSodium(responseJson) {

            if (responseJson.totalNutrients.NA) {
                let sodium = responseJson.totalNutrients.NA.quantity;
                totalSodiumList.push(sodium);
                return totalSodium = Math.ceil(totalSodiumList.reduce((a, b) => a + b))   
            } 
        }

        printInfo(totalCarbs, totalSugars, totalProtein, totalSaturated, totalCholesterol, totalSodium) {
        
            const pTotalCarbs = totalCarbs;
            const pTotalSugars = totalSugars;
            const pTotalProtein = totalProtein;
            const pTotalSaturated = totalSaturated;
            const pTotalCholesterol = totalCholesterol;
            const pTotalSodium = totalSodium;

            const html = `
                <button type="button" class="btn btn-secondary" style="width: 300px; margin-bottom: 20px;">
                    Total Carbs: <span class="badge bg-primary">${pTotalCarbs}g</span>
                </button>
                <button type="button" class="btn btn-secondary" style="width: 300px; margin-bottom: 20px;">
                    Total Sugar: <span class="badge bg-primary">${pTotalSugars}g</span>
                </button>
                <button type="button" class="btn btn-secondary" style="width: 300px; margin-bottom: 20px;">
                    Total Protein: <span class="badge bg-primary">${pTotalProtein}g</span>
                </button>
                <button type="button" class="btn btn-secondary" style="width: 300px; margin-bottom: 20px;">
                    Total Saturated Fat: <span class="badge bg-primary">${pTotalSaturated}g</span>
                </button>
                <button type="button" class="btn btn-secondary" style="width: 300px; margin-bottom: 20px;">
                    Total Cholesterol: <span class="badge bg-primary">${pTotalCholesterol}mg</span>
                </button>
                <button type="button" class="btn btn-secondary" style="width: 300px; margin-bottom: 20px;">
                    Total Sodium: <span class="badge bg-primary">${pTotalSodium}mg</span>
                </button>                                    
            `

            return $('#info').empty().append(html)
        }

        static count = (value) => `I'm used my API ${value} times.`

    }

    let meal = new CalcNutrition;


    ingredientEl.addEventListener('keypress', function(e) {

        if (e.keyCode == 13) {
            
            // form validation 
            if (ingredientEl.value === "") {
                return $(error).text("Please enter an ingredient.")
            }

            const ingredient = ingredientEl.value;

            const BASE_URL = 'https://api.edamam.com/api/nutrition-data';
            const url = `${BASE_URL}?app_id=${APP_ID}&app_key=${APP_KEY}&ingr=${ingredient}`;
        
            //API call
            fetch(url)
        
                .then(function(data) {
                    // console.log(data);
                    return data.json();
                })
        
                .then(function(responseJson) {
                    // console.log(responseJson);
                    meal.stageIngredient(ingredient); 
                    meal.getTotalCarbs(responseJson);
                    meal.getTotalSugar(responseJson);
                    meal.getTotalProtein(responseJson);
                    meal.getTotalSaturated(responseJson);
                    meal.getTotalCholesterol(responseJson);
                    meal.getTotalSodium(responseJson);
                    meal.printInfo(totalCarbs, totalSugars, totalProtein, totalSaturated, totalCholesterol, totalSodium);
                    $(ingredientEl).val("");
                    $('#error').empty();
                })

                .catch(function(error) {
                    // console.log(error);
                    $('#error').empty().text("Please try again.")
                    $('ol>li:last-child').remove() 
                    $(ingredientEl).val("");               
                });
        }
    });

    resetEl.addEventListener('click', function() {
        $(ingredientEl).val("");
        $('#info').empty();
        $('#stage-ingredient').empty();

        setTimeout( function() { 
            $('#error').empty();
        }, 3000);
    });

});